import { useEffect, useState } from "react";
import Swal from 'sweetalert2';
import Button from "./components/Button";
import Visualizer from "./components/Visualizer";
const SortMethod = require('./functions/SortMethod');

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function App() {
  const [sortValues, setSortValues] = useState([]);
  const [maximumValue] = useState(100);
  const [count, setCount] = useState(20);
  const [speed, setSpeed] = useState(100);
  const [sorting, setSorting] = useState(undefined);

  // Generate random values
  const randomizeValues = () => {
    let updatedSortValues = [];

    for (let idx = 0; idx < count; idx++) {
      updatedSortValues.push(Math.round(Math.random() * count) + 5);
    }

    return updatedSortValues;
  }

  // Generate random values on first render
  useEffect(() => {
    global.STOP_SORT = true;
    setSortValues(randomizeValues());
  }, []);

  const handleNewArrayButton = () => {
    global.STOP_SORT = true;
    setSortValues(randomizeValues());
  }

  // Sort values onClick
  const handleSortArrayButton = (id) => {
    if (sorting !== undefined) {
      return Swal.fire({
        title: 'Information',
        text: `${capitalizeFirstLetter(sorting)} sort is still in progress`,
        icon: 'info',
        confirmButtonText: 'Ok'
      });
    }

    global.STOP_SORT = false;
    setSorting(id);

    if (id === "bubble") {
      SortMethod.bubbleSort(sortValues, speed, (updatedValues) => setSortValues([...updatedValues]), () => setSorting(undefined));
    }
    else if (id === "selection") {
      SortMethod.selectionSort(sortValues, speed, (updatedValues) => setSortValues([...updatedValues]), () => setSorting(undefined));
    }
    else {
      Swal.fire({
        title: 'Oops!',
        text: `${capitalizeFirstLetter(sorting)} has yet to be implemented`,
        icon: 'error',
        confirmButtonText: 'Ok'
      });

      setSorting(undefined);
    }
  }

  // Stop sort onClick
  const handleStopArrayButton = () => {
    if (global.STOP_SORT) {
      return Swal.fire({
        title: 'Warning',
        text: `Sorting has already been stopped.`,
        icon: 'warning',
        confirmButtonText: 'Ok'
      });
    }

    global.STOP_SORT = true;
    setSorting(undefined);
  }

  // Change sort speed
  const handleSortSpeed = (e) => {
    setSpeed(e.target.value);
  }

  // Change array size with range
  const handleSizeChange = (e) => {
    setCount(e.target.value);
    setSortValues(randomizeValues());

  }

  return (
    <main className="h-full">
      <section className="mx-auto border-[1px] bg-slate-200">

        {/* Title */}
        <section className="w-full">
          <h1 className="text-center text-2xl py-2 text-white font-bold bg-purple-700 ">Javascript Sorting Visualizer</h1>
        </section >

        {/* Operations */}
        <section className="flex flex-col md:flex-row mx-auto pt-4" >
          <span className="flex basis-2/6">
            <Button className="w-full md:w-auto" onClick={handleNewArrayButton}>Regenerate Array</Button>
          </span>

          <span className="flex flex-col md:flex-row basis-2/6 place-content-center">
            <div className="flex flex-col px-2">
              <label htmlFor="speed">Speed ({speed}ms)</label>
              <input id="speed" type="range" min={1} max={10 * 1000} onChange={handleSortSpeed} />
            </div>

            <Button className="w-auto bg-red-500 border-red-600 hover:border-red-800" onClick={handleStopArrayButton}>Stop Sort</Button>

            <div className="flex flex-col px-2">
              <label htmlFor="speed">Array Size ({count})</label>
              <input id="speed" type="range" min={10} max={maximumValue} onChange={handleSizeChange} />
            </div>
          </span>

          <span className="basis-2/6 flex flex-col md:flex-row place-content-end">
            <Button id="bubble" onClick={handleSortArrayButton}>Bubble Sort</Button>
            <Button id="quick" onClick={handleSortArrayButton}>Quick Sort</Button>
            <Button id="selection" onClick={handleSortArrayButton}>Selection Sort</Button>
            <Button id="insertion" onClick={handleSortArrayButton}>Insertion Sort</Button>
            <Button id="merge" onClick={handleSortArrayButton}>Merge Sort</Button>
          </span>
        </section >

        {/* Visuals */}
        <section className="pt-4">
          <Visualizer values={sortValues} />
        </section>
      </section >

      <section className="h-[70px] relative w-full top-full footer">
        <footer className="flex flex-col justify-center h-full bg-gray-800 text-2xl text-white text-center font-bold my-auto">
          <p>
            Developed by: &nbsp;
            <a href="https://koladeolorunnife.com/#projects" target="_blank" rel="noreferrer" className="text-blue-400 hover:text-blue-700">
              Kolade Olorunnife
            </a>
          </p>
        </footer>
      </section>
    </main>
  );
}

export default App;
