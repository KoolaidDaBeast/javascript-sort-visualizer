module.exports = {
    wait: function (v) {
        const promise = new Promise((myResolve, myReject) => {
            setTimeout(() => {
                myResolve();
            }, v);
        });

        return promise;
    },

    bubbleSort: async function (arr, speed, onChange, onComplete) {
        for (var i = 0; i < arr.length; i++) {
            if (global.STOP_SORT) break;

            for (var j = 0; j < (arr.length - i - 1); j++) {
                if (global.STOP_SORT) break;

                if (arr[j] > arr[j + 1]) {
                    var temp = arr[j]
                    arr[j] = arr[j + 1]
                    arr[j + 1] = temp;

                    onChange(arr);
                    await this.wait(speed);
                }
            }
        }

        onComplete();
    },

    selectionSort: async function (arr, speed, onChange, onComplete) {
        for (let i = 0; i < arr.length; i++) {
            if (global.STOP_SORT) break;

            let lowest = i;

            for (let j = i + 1; j < arr.length; j++) {
                if (global.STOP_SORT) break;

                if (arr[j] < arr[lowest]) {
                    lowest = j;
                }
            }

            if (lowest !== i) {
                [arr[i], arr[lowest]] = [arr[lowest], arr[i]];

                onChange(arr);
                await this.wait(speed);
            }
        }

        onComplete();
    }

}
