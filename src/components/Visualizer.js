import React from 'react'

export default function Visualizer({ values }) {
    return (
        <div className='flex w-full border-2 border-black'>
            {values.map((value, idx) => (
                idx !== values.length - 1 ?
                    <div key={idx} className='relative min-w-[1px] w-full mr-[1px] bg-purple-500' style={{ height: `${value * 3.5}px` }}>
                        <div className='absolute bottom-[0] w-full text-center text-xs'>
                            {value}
                        </div>
                    </div>
                    :
                    <div key={idx} className='relative min-w-[1px] w-full bg-purple-500' style={{ height: `${value * 3.5}px` }}>
                        <div className='absolute bottom-[0] w-full text-center text-xs'>
                            {value}
                        </div>
                    </div>
            ))}
        </div>
    )
}
