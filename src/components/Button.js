import React from 'react'

export default function Button({ id = "", className = "", children, onClick = () => { } }) {
    return (
        <button className={"rounded px-2 mx-2 my-2 md:my-0 border-2 border-purple-500 bg-purple-400 hover:border-purple-800 text-white " + className} onClick={() => onClick(id)}>
            {children}
        </button>
    )
}
